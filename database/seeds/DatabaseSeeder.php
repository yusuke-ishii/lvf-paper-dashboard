<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        DB::table('users')->delete();
        $user1 = App\User::create([
            // 'id'         => 1,
            'name'   => 'jet-auth',
            'email'      => 'jwt-test@example.com',
            'password'   => bcrypt('password'),
        ]);
        $user2 = App\User::create([
            // 'id'         => 2,
            'name'   => 'jet-auth',
            'email'      => 'jwt-test2@example.com',
            'password'   => bcrypt('password'),
        ]);

    }
}
