# LVF-P Dashboard System Boilerplate

Laravel, Vue.js, PythonFire, VuePaperDashboard を統合したダッシュボードシステムの雛形です。

このリポジトリをクローンして、
```
$ composer install
$ yarn install
$ yarn run dev
$ php artisan serve
```
の順にコマンドを実行後、localhost:8000にアクセスすると、VuePaperDashboardの画面を表示できます。
VuePaperDashboardのソースは Laravel Mix でビルドできるように組み込まれています。


######VuePaperDashboardのリンク
>Github: https://github.com/cristijora/vue-paper-dashboard

>Demo: https://cristijora.github.io/vue-paper-dashboard/#/admin/overview
